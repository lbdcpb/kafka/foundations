# Apache Kafka - Advanced Learning Path

## Introducción

El presente documento propone llevar al siguiente nivel lo visto en la ruta de aprendizaje básica de Apache Kafka.

## Material de lectura

En castellano:

- Este post fue escrito en asociación con Intuit para compartir aprendizajes, mejores prácticas y recomendaciones para ejecutar un clúster de Apache Kafka en AWS. Gracias a Vaishak Suresh y a sus compañeros de Intuit por sus aportaciones y apoyo. _Continuar leyendo en [Mejores prácticas para ejecutar Apache Kafka en AWS](https://aws.amazon.com/es/blogs/aws-spanish/best-practices-for-running-apache-kafka-on-aws/)._

En inglés:

- Blog de [Kai Waehner - Technology Evangelist – Big Data Analytics – Middleware – Apache Kafka](https://www.kai-waehner.de).

## Use cases

![use cases](/images/Event-Streaming-Platform-with-Apache-Kafka-Value-per-Use-Case--1024x582.png) 

_credits: [Kai Waehner - use cases](https://www.kai-waehner.de/?s=use+case)_

- Apache Kafka is an event streaming platform. It provides messaging, persistence, data integration, and data processing capabilities. High scalability for millions of messages per second, high availability including backward-compatibility and rolling upgrades for mission-critical workloads, and cloud-native features are some of the capabilities. _Read more at [Use Cases and Architectures for Apache Kafka across Industries](https://www.kai-waehner.de/blog/2020/10/20/apache-kafka-event-streaming-use-cases-architectures-examples-real-world-across-industries/)._

- Event streaming with Apache Kafka at the edge is not cutting edge anymore. It is a common approach to providing the same open, flexible, and scalable architecture at the edge as in the cloud or data center. Possible locations for a Kafka edge deployment include retail stores, cell towers, trains, small factories, restaurants, etc. _Read more at [Use Cases and Architectures for Kafka at the Edge](https://www.kai-waehner.de/blog/2020/10/14/use-cases-architectures-apache-kafka-edge-computing-industrial-iot-retail-store-cell-tower-train-factory/)._

- I won’t do yet another long introduction about the added value of real-time data. The public sector is not different: Real-time data beats slow data in almost every use case! Here are a few examples _Read more at [Apache Kafka in the Public Sector – Blog Series about Use Cases and Architectures](https://www.kai-waehner.de/blog/2021/10/07/apache-kafka-public-sector-part-1-data-in-motion-use-cases-architectures-examples/)._

- This post dives deeper into architectural questions and how collaboration with 3rd party services can look from the government’s perspective and public administration of a smart city. _Read more at [Apache Kafka in the Public Sector – Part 2: Smart City](https://www.kai-waehner.de/blog/2021/10/12/apache-kafka-public-sector-government-part-2-smart-city-iot-transportation-mobility-services/)._

- We talk a lot about customer 360 and customer experience in the private sector. The public sector is different. Increasing revenue is usually not the primary motivation. For that reason, most countries’ citizen-related processes are terrible experiences. _Read more at [Apache Kafka in the Public Sector – Part 3: Government and Citizen Services](https://www.kai-waehner.de/blog/2021/10/15/apache-kafka-public-sector-part-3-government-citizen-services/)._

- The energy sector is different in countries and even states. Public utilities are subject to public control and regulation, ranging from local community-based groups to statewide government monopolies. Hence, some markets are private businesses, or entirely controlled by the government, or a mix of both. _Read more at [Apache Kafka in the Public Sector – Part 4: Energy and Utilities](https://www.kai-waehner.de/blog/2021/10/18/apache-kafka-public-sector-part-4-energy-utilities-smart-grid/)._

- National security or national defense is the security and defense of a nation-state, including its citizens, economy, and institutions, is a duty of government. _Read more at [Apache Kafka in the Public Sector – Part 5: National Security and Defense](https://www.kai-waehner.de/blog/2021/10/20/apache-kafka-public-sector-part-5-national-security-cybersecurity-defense-military/)._

- The following shows some of the use cases I have seen in the field in pharma and life sciences. Many of them have in common that they are not new. But event streaming at scale in real time can help improve the processes and allow innovative new applications. Therefore Apache Kafka is a perfect fit for the Pharma and Life Science industry. _Read more at [Apache Kafka and Event Streaming in Pharma and Life Sciences](https://www.kai-waehner.de/blog/2020/05/19/apache-kafka-event-streaming-pharmaceuticals-pharma-life-sciences-use-cases-architecture/)._

- The retail industry is completely changing these days. Consequently, traditional players have to disrupt their business to stay competitive. New business models, great customer experience, and automated real-time supply chain processes are mandatory. Event Streaming with Apache Kafka plays a key role in this evolution of re-inventing the retail business. _Read more at [Use Cases for Apache Kafka in Retail](https://www.kai-waehner.de/blog/2021/01/28/apache-kafka-in-retail-use-cases-architecture-case-studies-examples/)._

## Material audiovisual

En castellano:

- [Confluent Schema Registry](https://www.youtube.com/watch?v=YAltICN4JLo&list=PL2yjEVbRSX7Vo1qtXYWbWR1BbWV_pBVK6&index=55)

- [PubSub y Kafka](https://www.youtube.com/watch?v=6T2TwZIWOEg)

En inglés:

- [Apache Kafka Connect](https://www.youtube.com/playlist?list=PLt1SIbA8guutTlfh0J7bGboW_Iplm6O_B)

- [Apache Kafka - Confluent Schema Registry & REST Proxy](https://www.youtube.com/playlist?list=PLt1SIbA8guuu9LAYuOuuYlFbSoE4fgfK1)

- [Apache Kafka Cluster Setup & Administration](https://www.youtube.com/playlist?list=PLt1SIbA8guuuArJSY9AIPmQL-oIBH7why)

- [Apache Kafka Security (SSL SASL Kerberos ACL)](https://www.youtube.com/playlist?list=PLt1SIbA8guusMatdciotF-WyG4IMBT1EG)

- [ksqlDB and Stream Processing Tutorials | ksqlDB 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH3ulxsOf5g43_QiB-HOg5_Y)

- [Data Mesh and Data Domains Tutorials | Data Mesh 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH0L8ahQYbyBFkGc6a949-Lj)

- [Kafka Connect Tutorials | Kafka Connect 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH0uIC2F0M1_FsVUsx8j3ekm)

- [Kafka Security 101 | Secure and Maintain Your Apache Kafka® Systems](https://www.youtube.com/playlist?list=PLa7VYi0yPIH2t3_wc1tm1rHDO9tbtfX1T)

## Cursos en línea

En castellano:

- (TBD)

En inglés:

- :star: :free: [Confluent - Administrator Learning Path](https://training.confluent.io/channeldetail/administrator-learning-path)

- :star: :free: [Confluent - Security Learning Path](https://training.confluent.io/channeldetail/security-learning-path)

- :moneybag: [Udemy - Apache Kafka Series - Kafka Connect Hands-on Learning](https://www.udemy.com/course/kafka-connect/)

- :moneybag: [Udemy - Apache Kafka Series - Kafka Cluster Setup & Administration](https://www.udemy.com/course/kafka-cluster-setup/)

- :moneybag: [Udemy - Apache Kafka Series - Confluent Schema Registry & REST Proxy](https://www.udemy.com/course/confluent-schema-registry/)

- :moneybag: [Udemy - Apache Kafka Series - KSQL on ksqlDB for Stream Processing](https://www.udemy.com/course/kafka-ksql/)

- :moneybag: [Udemy - Apache Kafka Series - Kafka Monitoring & Operations](https://www.udemy.com/course/kafka-monitoring-and-operations/)

- :moneybag: [Udemy - Apache Kafka Series - Kafka Security | SSL SASL Kerberos ACL](https://www.udemy.com/course/apache-kafka-security/)
