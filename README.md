# Foundations

## Documentantion

- [Intro](docs/intro/README.md): Understand where Kafka fits in the Big Data space, Kafka Architecture, Kafka Cluster and how to Configure one.

- [Producer](docs/producer/README.md): Work with different Kafka Producer APIs.

- [Consumer](docs/consumer/README.md): Process messages from Kafka with Consumer, run Kafka Consumer and subscribe to Topics.

- [Internals](docs/internals/README.md): Tuning Kafka to meet your high-performance needs.

## Links

- [Top 10 Courses to Learn Apache Kafka in 2022 | Best Kafka Courses for Beginners](https://medium.com/javarevisited/top-10-apache-kafka-online-training-courses-and-certifications-621f3c13b38c)

- [Edureka - Apache Kafka Certification Training Course](https://www.edureka.co/kafka-certification-training)

- [Simplilearn - Apache Kafka Certification Training](https://www.simplilearn.com/apache-kafka-certification-training-course)

- [Udemy - Apache Kafka Series by Conduktor Kafkademy](https://www.udemy.com/user/conduktor/)

- [Pluralsight - Getting Started with Apache Kafka](https://www.pluralsight.com/courses/apache-kafka-getting-started)

- [Coursera - Vivek Sarkar's courses](https://www.coursera.org/instructor/vivek-sarkar)

- [Confluent - Self-Paced Training](https://training.confluent.io/packagedetail/confluent-education-free-self-paced)

- [Kafka articles by enmilocalfunciona.com - spanish content](https://www.enmilocalfunciona.io/tag/kafka/)

- [Kai Waehner - Technology Evangelist – Big Data Analytics – Middleware – Apache Kafka](https://www.kai-waehner.de)

## Videos & Playlists

### Paradigma Digital (spanish content)

- [Descubriendo Kafka con Confluent](https://www.youtube.com/watch?v=6Z41_RvEDdY&list=PL2yjEVbRSX7Vo1qtXYWbWR1BbWV_pBVK6&index=31)

- [¿Qué es y cómo funciona Apache Kafka?](https://www.youtube.com/watch?v=UNMML-YLAxs&list=PL2yjEVbRSX7Vo1qtXYWbWR1BbWV_pBVK6&index=39)

- [Stream Processing en Kafka](https://www.youtube.com/watch?v=8W6rFRk5SZE&list=PL2yjEVbRSX7Vo1qtXYWbWR1BbWV_pBVK6&index=45)

- [Confluent Schema Registry](https://www.youtube.com/watch?v=YAltICN4JLo&list=PL2yjEVbRSX7Vo1qtXYWbWR1BbWV_pBVK6&index=55)

- [PubSub y Kafka](https://www.youtube.com/watch?v=6T2TwZIWOEg)

### Stephane Maarek

- [Apache Kafka for Beginners](https://www.youtube.com/playlist?list=PLt1SIbA8guusxiHz9bveV-UHs_biWFegU)

- [Apache Kafka Connect](https://www.youtube.com/playlist?list=PLt1SIbA8guutTlfh0J7bGboW_Iplm6O_B)

- [Apache Kafka Streams for Data Processing](https://www.youtube.com/playlist?list=PLt1SIbA8guusO6cvjVNmjFDio_Qy-fC5j)

- [Apache Kafka - Confluent Schema Registry & REST Proxy](https://www.youtube.com/playlist?list=PLt1SIbA8guuu9LAYuOuuYlFbSoE4fgfK1)

- [Apache Kafka Cluster Setup & Administration](https://www.youtube.com/playlist?list=PLt1SIbA8guuuArJSY9AIPmQL-oIBH7why)

- [Apache Kafka Security (SSL SASL Kerberos ACL)](https://www.youtube.com/playlist?list=PLt1SIbA8guusMatdciotF-WyG4IMBT1EG)

### Confluent

- [Apache Kafka® Tutorials | Kafka 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH0KbnJQcMv5N9iW8HkZHztH)

- [Kafka Streams Tutorials | Kafka Streams 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH35IrbJ7Y0U2YLrR9u4QO-s)

- [ksqlDB and Stream Processing Tutorials | ksqlDB 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH3ulxsOf5g43_QiB-HOg5_Y)

- [Data Mesh and Data Domains Tutorials | Data Mesh 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH0L8ahQYbyBFkGc6a949-Lj)

- [Kafka Connect Tutorials | Kafka Connect 101](https://www.youtube.com/playlist?list=PLa7VYi0yPIH0uIC2F0M1_FsVUsx8j3ekm)

- [Kafka Security 101 | Secure and Maintain Your Apache Kafka® Systems](https://www.youtube.com/playlist?list=PLa7VYi0yPIH2t3_wc1tm1rHDO9tbtfX1T)

## Credits

- [Foundation icons created by Marz Gallery - Flaticon](https://www.flaticon.com/free-icons/foundation)
