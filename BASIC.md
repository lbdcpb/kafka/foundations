# Apache Kafka - Basic Learning Path

## Introducción

El presente documento propone una ruta de aprendizaje inicial de Apache Kafka.

## Material de lectura

En castellano:

- Apache Kafka es una plataforma distribuida para la transmisión de datos que permite no solo publicar, almacenar y procesar flujos de eventos de forma inmediata, sino también suscribirse a ellos. _Continuar leyendo en [¿Qué es Apache Kafka?](https://www.redhat.com/es/topics/integration/what-is-apache-kafka)_

- El objetivo que se persigue con esta serie de artículos es "aprender" a utilizar de la forma más práctica posible la plataforma Apache Kafka con todas sus características y particularidades. _Continuar leyendo en [Aprendiendo Apache Kafka](https://www.enmilocalfunciona.io/aprendiendo-apache-kafka-parte-1/)._

En inglés:

- The definition of big data is data that contains greater variety, arriving in increasing volumes and with more velocity. This is also known as the three Vs. _Read more at [What is Big Data?](https://www.oracle.com/big-data/what-is-big-data/)_

- Big data analytics is the often complex process of examining big data to uncover information -- such as hidden patterns, correlations, market trends and customer preferences -- that can help organizations make informed business decisions. _Read more at [Understand why Big Data Analytics is important](https://www.techtarget.com/searchbusinessanalytics/definition/big-data-analytics)._

- ZooKeeper is used in distributed systems for service synchronization and as a naming registry.  When working with Apache Kafka, ZooKeeper is primarily used to track the status of nodes in the Kafka cluster and maintain a list of Kafka topics and messages. _Read more at [What is ZooKeeper & How Does it Support Kafka?](https://dattell.com/data-architecture-blog/what-is-zookeeper-how-does-it-support-kafka/)_

- To understand any technology, we must understand why it was created/ invented. Apache Kafka was originally developed at LinkedIn in 2010 as a means of handling high-volume, high-throughput, real-time data feeds. LinkedIn was looking for a way to handle the high volume of activity on its platform, which included user updates, relationship updates, and other data that needed to be processed in real-time. _Read more at [Kafka in Action: A Practical Guide to Getting Started with Kafka](https://blog.devgenius.io/kafka-in-action-a-practical-guide-to-getting-started-with-kafka-7be5074a30c2)._

## Casos de uso

![use cases](/images/Event-Streaming-Platform-with-Apache-Kafka-Value-per-Use-Case--1024x582.png)

_credits: [Kai Waehner - use cases](https://www.kai-waehner.de/?s=use+case)_

- Event Streaming is happening all over the world. This blog post explores real-life examples across industries for use cases and architectures leveraging Apache Kafka. Learn about architectures for real-world deployments from Audi, BMW, Disney, Generali, Paypal, Tesla, Unity, Walmart, William Hill, and more. Use cases include fraud detection, mainframe offloading, predictive maintenance, cybersecurity, edge computing, track&trace, live betting, and much more. _Read more at [Real World Examples and Use Cases for Apache Kafka](https://dzone.com/articles/real-world-examples-and-use-cases-for-apache-kafka)._

- Apache Kafka and Event Streaming are two of the most relevant buzzwords in tech these days. Ever wonder what the predicted TOP 5 Event Streaming Architectures and Use Cases for 2021 are? _Read more at [Top 5 Event Streaming Use Cases for 2021 with Apache Kafka](https://www.kai-waehner.de/blog/2020/12/16/top-5-event-streaming-apache-kafka-use-cases-2021-edge-hybrid-cloud-cybersecurity-machine-learning-service-mesh/)._

- Apache Kafka and Event Streaming are two of the most relevant buzzwords in tech these days. Ever wonder what my predicted TOP 5 Event Streaming Architectures and Use Cases for 2022 are to set data in motion? _Read more at [Top 5 Apache Kafka Use Cases for 2022](https://www.kai-waehner.de/blog/2021/12/02/top-5-apache-kafka-use-cases-for-2022-kappa-cybersecurity-customer-360-edge-cloud/)._

- Data Streaming is one of the most relevant buzzwords in tech to build scalable real-time applications in the cloud and innovative business models. Do you wonder about my predicted TOP 5 data streaming trends in 2023 to set data in motion? _Read more at [Top 5 Data Streaming Trends for 2023](https://www.kai-waehner.de/blog/2022/12/15/top-5-data-streaming-trends-for-2023-with-apache-kafka/)_

## Material audiovisual

En castellano:

- [Descubriendo Kafka con Confluent](https://www.youtube.com/watch?v=6Z41_RvEDdY&list=PL2yjEVbRSX7Vo1qtXYWbWR1BbWV_pBVK6&index=31)

- [¿Qué es y cómo funciona Apache Kafka?](https://www.youtube.com/watch?v=UNMML-YLAxs&list=PL2yjEVbRSX7Vo1qtXYWbWR1BbWV_pBVK6&index=39)

- [Stream Processing en Kafka](https://www.youtube.com/watch?v=8W6rFRk5SZE&list=PL2yjEVbRSX7Vo1qtXYWbWR1BbWV_pBVK6&index=45)

En inglés:

- [Kafka as a Platform: the Ecosystem from the Ground Up](https://talks.rmoff.net/8OCgKp/kafka-as-a-platform-the-ecosystem-from-the-ground-up)

- [System Design: Why is Kafka fast?](https://www.youtube.com/watch?v=UNUz1-msbOM)

- :star: [Apache Kafka On the Go | Kafka Concepts for Beginners](https://www.youtube.com/playlist?list=PLa7VYi0yPIH3OY8zW7EU_pds2IKDHAfRj)

- [Apache Kafka for Beginners](https://www.youtube.com/playlist?list=PLt1SIbA8guusxiHz9bveV-UHs_biWFegU)

- [Apache Kafka Streams for Data Processing](https://www.youtube.com/playlist?list=PLt1SIbA8guusO6cvjVNmjFDio_Qy-fC5j)

- [Confluent Customers and Use Cases](https://www.youtube.com/playlist?list=PLa7VYi0yPIH2oAWJDpBuue3w8RsMa_H9L)

## Aprendizaje en línea

En castellano:

- :moneybag: [Udemy - Comienza con Kafka: Curso de Apache Kafka desde cero](https://www.udemy.com/course/apache-kafka-esp/)

- :moneybag: [Udemy - Apache Kafka con Java, Spring framework y AWS](https://www.udemy.com/course/apache-kafka-es/)

- :moneybag: [Datademia - Comienza con Kafka - Aprende a trabajar con Apache Kafka](https://cursos.datademia.es/p/p-apache-kafka)

En inglés:

- Básicos

  - :star: :free: [Confluent - Apache Kafka Fundamentals](https://training.confluent.io/channeldetail/apache-kafka-fundamentals-and-accreditation)

  - :star: :free: [Confluent - Developer Learning Path](https://training.confluent.io/channeldetail/developer--learning-path)

  - :moneybag: [Pluralsight - Getting Started with Apache Kafka](https://www.pluralsight.com/courses/apache-kafka-getting-started)

  - :moneybag: [Udemy - Apache Kafka Series - Learn Apache Kafka for Beginners v3](https://www.udemy.com/course/apache-kafka/)

  - :moneybag: [Udemy - Apache Kafka Series - Kafka Streams for Data Processing](https://www.udemy.com/course/kafka-streams/)

- Integrales

  - :moneybag: [Edureka - Apache Kafka Certification Training Course](https://www.edureka.co/kafka-certification-training)

  - :moneybag: [Simplilearn - Apache Kafka Certification Training](https://www.simplilearn.com/apache-kafka-certification-training-course)

- Para profundizar conceptos

  - :free: [Coursera - Parallel Programming in Java](https://www.coursera.org/learn/parallel-programming-in-java)

  - :free: [Coursera - Concurrent Programming in Java](https://www.coursera.org/learn/concurrent-programming-in-java)

  - :free: [Coursera - Distributed Programming in Java](https://www.coursera.org/learn/distributed-programming-in-java)
